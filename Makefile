SOURCES = $(wildcard src/*.tex)
TARGETS = $(basename $(SOURCES))

OUT_DIR = out
TMP_DIR = tmp

COMMAND = pdflatex
FLAGS = -halt-on-error -output-directory $(TMP_DIR)

all: install

install: $(TARGETS)
	mkdir -p $(OUT_DIR)
	mv $(TMP_DIR)/*.pdf $(OUT_DIR)

ci: $(TARGETS)
	mv $(TMP_DIR)/*.pdf .

$(TARGETS): %: %.pdf

%.pdf: %.tex %.toc
	mkdir -p $(TMP_DIR)
	$(COMMAND) $(FLAGS) $<

%.toc: %.tex
	mkdir -p $(TMP_DIR)
	$(COMMAND) $(FLAGS) $<

clean:
	$(RM) -r $(TMP_DIR)
	$(RM) -r $(OUT_DIR)
	$(RM) *.pdf
