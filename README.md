# LaTeX PDF Booklets
This repository provides a LaTeX source file and the commands required to generate booklets from a PDF source file.

While Adobe's PDF application allows printing PDF document as booklets, it adds a 0.5in border around the entire page, causing pages to be printed smaller and off-center. The LaTeX pdfpages package will layout the pages edge-to-edge, meaning that documents that have already been laid out with correct margins will print out as they appear in the source.

## Set Up

### LaTeX Installation
The project expects that a distribution of LaTeX providing the `pdflatex` binary has been installed.

Linux distributions will typically have versions of "texlive" available.
For example, on Ubuntu, the program can be installed with `apt install texlive`.

### Provide A Source PDF

The booklet tex file expects a file named `source.pdf` in the `./src/pdf/` directory.

For best aesthetic results, consider providing a PDF with pages of booklet size (5.5 x 8.5).

## Make The Booklet
Run the `make` command in the project's root directory.
The make command will run the `pdflatex` command for each `*.tex` file in the source directory.

## Print The Booklet
The booklet will be generated and placed into the `out` directory.

When printing the booklet, the pages should be flipped on the "long edge".

## Clean Up
Run the `make clean` command in the project's root directory.
The clean command will remove all temporary and output directories and files. Note that it will also remove any pdf files in the project's root directory.

Following is an example run:
```
# make clean
rm -f -r tmp
rm -f -r out
rm -f *.pdf
```
